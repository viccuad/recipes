Fabada (Dad's recipe)
======

5-6 servings

### Ingredients

* 1 big spoon of olive oil (extra virgin)
* 3 garlic cloves
* 1 onion (middle sized)
* 500g big white beans (Spanish, La granja type) 
* 2 spanish blood sausages (morcilla)
* 250g of bacon 
* 250g of ham
* 250g of spicy chorizo
* 1-2 big spoons of paprika sweet powder

### Preparations

The night before, have a look at the beans, remove any that look bad, and put
them in water for them to get hydrated back.
Also, if you are not sure of the quality of the meat, put the bacon and ham also
in water to desalinate them. Don't put the blood sausages or the chorizo, though.
Leave everything in water for 8-12 hours.

### Directions

We are going to try and do the fabada "quicker", hence the pressure cooker. You
could do it without the pressure cooker, but then you should cook it always on
medium heat for 4h.

Discard the water from the beans and the meat. Grab your amazing pressure
cooker, and put the beans inside. Cover them with water until the water is above
them for 1-2 fingers.

Peal and cut the chorizo in thick slices 2 finger thick, make cuts on the bacon
and ham as you would do on watermelon slices, so they are open but not
separated. Don't cut the morcillas.
Then, gently settle the meat on top (so it's covered 3/4), 3 cleaned garlic
cloves, and the cleaned onion. Sprinkle around 1 big spoon of olive oil on top.
Never stir the beans or anything.

Close it, put it on heat until it's pressure cooking, and then let it be on
medium heat for 25 mins.

Once the time has passed, put the cooker over fresh water to cold it and break
the cooking, but be careful to not move or shuffle it around: you don't want the
beans to peel themselves against each other.

Open it and put it back on medium heat again. Gently remove any white foam in
the surface with a spoon. You can also remove red fat as you see fit (but it's a
pity for the taste).

Take half a glass of beans, liquid and half the onion and mash it on a minipimer
or the like until you get a soup. Add it back into the fabada pot, and shake the
whole pot gently like a car wheel until everything is mixed.

Add 1 big spoon of paprika sweet powder. From now on, let the fabada cook on
medium to high heat for 35 mins for the tastes to mix. Keep remove white foam as
needed.

Slice 1 garlic clove and lightly fry it on olive oil. Once the fabada is cooked,
take out all the meat into a plate for people to serve from, add the garlic
clove and the oil to the pot, stir a bit, taste and add salt if needed.

Serve plates with beans plus liquid, and add meat as people like. Add a spoon of
apple vinegar to bring up the flavours and reduce the music.
